package com.pointlion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * 
 * @author ry
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class,
        org.flowable.spring.boot.FlowableSecurityAutoConfiguration.class})
public class Application
{
    public static void main(String[] args)
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(Application.class, args);
        System.out.println("             ___                            ___                                                                                    \n" +
                "            (   )                          (   )                                                                                   \n" +
                "    .--.     | |_       .---.   ___ .-.     | |_           .--.     ___  ___    .--.      .--.      .--.       .--.        .--.    \n" +
                "  /  _  \\   (   __)    / .-, \\ (   )   \\   (   __)       /  _  \\   (   )(   )  /    \\    /    \\    /    \\    /  _  \\     /  _  \\   \n" +
                " . .' `. ;   | |      (__) ; |  | ' .-. ;   | |         . .' `. ;   | |  | |  |  .-. ;  |  .-. ;  |  .-. ;  . .' `. ;   . .' `. ;  \n" +
                " | '   | |   | | ___    .'`  |  |  / (___)  | | ___     | '   | |   | |  | |  |  |(___) |  |(___) |  | | |  | '   | |   | '   | |  \n" +
                " _\\_`.(___)  | |(   )  / .'| |  | |         | |(   )    _\\_`.(___)  | |  | |  |  |      |  |      |  |/  |  _\\_`.(___)  _\\_`.(___) \n" +
                "(   ). '.    | | | |  | /  | |  | |         | | | |    (   ). '.    | |  | |  |  | ___  |  | ___  |  ' _.' (   ). '.   (   ). '.   \n" +
                " | |  `\\ |   | ' | |  ; |  ; |  | |         | ' | |     | |  `\\ |   | |  ; '  |  '(   ) |  '(   ) |  .'.-.  | |  `\\ |   | |  `\\ |  \n" +
                " ; '._,' '   ' `-' ;  ' `-'  |  | |         ' `-' ;     ; '._,' '   ' `-'  /  '  `-' |  '  `-' |  '  `-' /  ; '._,' '   ; '._,' '  \n" +
                "  '.___.'     `.__.   `.__.'_. (___)         `.__.       '.___.'     '.__.'    `.__,'    `.__,'    `.__.'    '.___.'     '.___.'   \n");

        System.out.println("欢迎使用点狮后台管理系统：http://www.dianshixinxi.com");
    }
}
