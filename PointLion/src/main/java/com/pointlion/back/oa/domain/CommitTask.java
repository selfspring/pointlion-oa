package com.pointlion.back.oa.domain;

import lombok.Data;

/***
 * @des
 * @author Ly
 * @date 2023/6/1
 */
@Data
public class CommitTask {

    private String taskId;

    private String insId;

    private String comment;

    private String billType;
}
