package com.pointlion.back.oa.controller;

import com.pointlion.back.oa.domain.FlowProcessInstance;
import com.pointlion.back.oa.service.FlowProcessInstanceService;
import com.pointlion.common.core.controller.BaseController;
import com.pointlion.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/***
 * @des
 * @author Ly
 * @date 2023/5/31
 */
@RestController
@RequestMapping("/oa/processInstance")
public class FlowProcessInsController extends BaseController {

    @Autowired
    private FlowProcessInstanceService flowProcessInstanceService;

    @RequestMapping("/deleteAllProcessInstance")
    public AjaxResult deleteAllProcessInstance(){
        flowProcessInstanceService.deleteAllProcessInstance();
        return success();
    }




    /*****
     * 提交流程
     * @param ins
     * @return
     * @throws Exception
     */
    @RequestMapping("/submitFlow")
    public AjaxResult submitFlow(@RequestBody FlowProcessInstance ins) throws Exception {
        flowProcessInstanceService.submitFlow(ins);
        return success();
    }


    /*****
     * 取回流程
     * @param ins
     * @return
     * @throws Exception
     */
    @RequestMapping("/cancleFlow")
    public AjaxResult cancleFlow(@RequestBody FlowProcessInstance ins) throws Exception {
        flowProcessInstanceService.cancleFlow(ins);
        return success();
    }
}
