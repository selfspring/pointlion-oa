package com.pointlion.back.oa.mapper;

import java.util.List;

import com.pointlion.back.oa.domain.VTasklist;

/**
 * 我的待办Mapper接口
 * 
 * @author pointLion
 * @date 2022-07-16
 */
public interface VTasklistMapper 
{
    /**
     * 查询我的待办
     * 
     * @param taskid 我的待办主键
     * @return 我的待办
     */
    public VTasklist selectVTasklistByTaskid(String taskid);

    /**
     * 查询我的待办列表
     * 
     * @param vTasklist 我的待办
     * @return 我的待办集合
     */
    public List<VTasklist> selectVTasklistList(VTasklist vTasklist);

    /****
     * 查询审批历史
     * @param insId
     * @return
     */
    public List<VTasklist> selectHisByInsId(String insId);


    /****
     * 查询待办任务列表
     * @param username
     * @return
     */
    public List<VTasklist> selectTodoTaskList(String username);
}
