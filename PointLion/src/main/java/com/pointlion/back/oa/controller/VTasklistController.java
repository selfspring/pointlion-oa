package com.pointlion.back.oa.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.pointlion.back.oa.domain.CommitTask;
import com.pointlion.back.oa.domain.FlowProcessInstance;
import com.pointlion.back.oa.service.VTasklistService;
//import com.pointlion.back.oa.domain.OaCustomFormApply;
//import com.pointlion.back.oa.service.OaTaskService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.pointlion.common.annotation.Log;
import com.pointlion.common.core.controller.BaseController;
import com.pointlion.common.core.domain.AjaxResult;
import com.pointlion.common.enums.BusinessType;
import com.pointlion.back.oa.domain.VTasklist;
import com.pointlion.common.utils.poi.ExcelUtil;
import com.pointlion.common.core.page.TableDataInfo;

/**
 * 我的待办Controller
 * 
 * @author pointLion
 * @date 2022-07-16
 */
@RestController
@RequestMapping("/oa/myTask")
public class VTasklistController extends BaseController
{
    @Autowired
    private VTasklistService vTasklistService;
    //@Autowired
    //private OaTaskService oaTaskService;


    /**
     * 查询我的待办列表
     */
    @PreAuthorize("@ss.hasPermi('oa:myTask:list')")
    @GetMapping("/list")
    public TableDataInfo list(VTasklist vTasklist)
    {
        startPage();
        List<VTasklist> list = vTasklistService.selectVTasklistList(vTasklist);
        return getDataTable(list);
    }

    /**
     * 导出我的待办列表
     */
    @PreAuthorize("@ss.hasPermi('oa:myTask:export')")
    @Log(title = "我的待办", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, VTasklist vTasklist)
    {
        List<VTasklist> list = vTasklistService.selectVTasklistList(vTasklist);
        ExcelUtil<VTasklist> util = new ExcelUtil<VTasklist>(VTasklist.class);
        util.exportExcel(response, list, "我的待办数据");
    }

    /**
     * 获取我的待办详细信息
     */
    @PreAuthorize("@ss.hasPermi('oa:myTask:query')")
    @GetMapping(value = "/{taskid}")
    public AjaxResult getInfo(@PathVariable("taskid") String taskid)
    {
        return AjaxResult.success(vTasklistService.selectVTasklistByTaskid(taskid));
    }


    /**
     * 查询审批历史
     */
    @PreAuthorize("@ss.hasPermi('oa:myTask:list')")
    @GetMapping("/getHisByInsId/{insId}")
    public AjaxResult getHisByInsId(@PathVariable("insId") String insId)
    {
        List<VTasklist> list = vTasklistService.selectHisByInsId(insId);
        return AjaxResult.success(list);
    }

    /****
     * 查看审批历史
     * @param flowProcessInstance
     * @return
     */
    @PostMapping("/getHisByBusinessId")
    public AjaxResult getHisByBusinessId(@RequestBody FlowProcessInstance flowProcessInstance)
    {
        List<VTasklist> list = vTasklistService.getHisByBusinessId(flowProcessInstance.getBusinessId(),flowProcessInstance.getBillType());
        return AjaxResult.success(list);
    }


    /**
     * 查询我的待办列表
     */
    @PreAuthorize("@ss.hasPermi('oa:myTask:list')")
    @GetMapping("/todoTaskList")
    public TableDataInfo todoTaskList()
    {
        startPage();
        List<VTasklist> list = vTasklistService.selectTodoTaskList(getLoginUser().getUsername());
        return getDataTable(list);
    }

    /**
     * 获取我的待办详细信息
     */
    //@GetMapping(value = "/getCustomFormByTaskId/{taskId}")
    //public AjaxResult getCustomFormByTaskId(@PathVariable("taskId") String taskId)
    //{
    //
    //    OaCustomFormApply oaCustomFormApply =  oaTaskService.getCustomFormByTaskId(taskId);
    //
    //    return AjaxResult.success(oaCustomFormApply);
    //}


    /******
     * 提交任务
     * @param
     * @return
     */
    @PostMapping(value = "/commitTask")
    public AjaxResult commitTask(@RequestBody CommitTask task){
        vTasklistService.commitTask(task.getTaskId(),task.getInsId(),task.getComment());
        return success();
    }


    /******
     * 驳回任务
     * @param
     * @return
     */
    @PostMapping(value = "/rejectTask")
    public AjaxResult rejectTask(@RequestBody CommitTask task){
        vTasklistService.rejectTask(task.getTaskId(),task.getInsId(),task.getComment());
        return success();
    }

}
