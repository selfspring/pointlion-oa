package com.pointlion.back.oa.service;

import java.util.List;
import com.pointlion.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.pointlion.back.oa.mapper.OaBdMeetingroomMapper;
import com.pointlion.back.oa.domain.OaBdMeetingroom;

/**
 * 会议室管理Service业务层处理
 * 
 * @author pointLion
 * @date 2023-05-29
 */
@Service
public class OaBdMeetingroomService
{
    @Autowired
    private OaBdMeetingroomMapper oaBdMeetingroomMapper;

    /**
     * 查询会议室管理
     * 
     * @param id 会议室管理主键
     * @return 会议室管理
     */
    public OaBdMeetingroom selectOaBdMeetingroomById(Long id)
    {
        return oaBdMeetingroomMapper.selectOaBdMeetingroomById(id);
    }

    /**
     * 查询会议室管理列表
     * 
     * @param oaBdMeetingroom 会议室管理
     * @return 会议室管理
     */
    public List<OaBdMeetingroom> selectOaBdMeetingroomList(OaBdMeetingroom oaBdMeetingroom)
    {
        return oaBdMeetingroomMapper.selectOaBdMeetingroomList(oaBdMeetingroom);
    }

    /**
     * 新增会议室管理
     * 
     * @param oaBdMeetingroom 会议室管理
     * @return 结果
     */
    public int insertOaBdMeetingroom(OaBdMeetingroom oaBdMeetingroom)
    {
        oaBdMeetingroom.setCreateTime(DateUtils.getNowDate());
        return oaBdMeetingroomMapper.insertOaBdMeetingroom(oaBdMeetingroom);
    }

    /**
     * 修改会议室管理
     * 
     * @param oaBdMeetingroom 会议室管理
     * @return 结果
     */
    public int updateOaBdMeetingroom(OaBdMeetingroom oaBdMeetingroom)
    {
        oaBdMeetingroom.setUpdateTime(DateUtils.getNowDate());
        return oaBdMeetingroomMapper.updateOaBdMeetingroom(oaBdMeetingroom);
    }

    /**
     * 批量删除会议室管理
     * 
     * @param ids 需要删除的会议室管理主键
     * @return 结果
     */
    public int deleteOaBdMeetingroomByIds(Long[] ids)
    {
        return oaBdMeetingroomMapper.deleteOaBdMeetingroomByIds(ids);
    }

    /**
     * 删除会议室管理信息
     * 
     * @param id 会议室管理主键
     * @return 结果
     */
    public int deleteOaBdMeetingroomById(Long id)
    {
        return oaBdMeetingroomMapper.deleteOaBdMeetingroomById(id);
    }
}
