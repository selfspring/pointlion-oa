package com.pointlion.common.constant;
/**
 * @author Ly
 * @Description 单据类型枚举类
 * @Classname BillTypeEnum
 * @Date 2023/06/02
 */
public enum BillTypeEnum {

    /**
     * 单据类型枚举
     */
    REIMBURSE("reimburse","报销申请单","oa_reimburse_apply","reimburse")
    ,REIMBURSE_BUS("reimburse_bus","差旅费报销单","oa_reimburse_apply","reimburse_bus")
    ,AGREEMENT("agreement","合同补充协议","ct_agreement","agreement")
    ,MATERIAL("material","物料","pl_material","")
    ,BUY_BILL("buy_bill","采购订单","pl_depot_bill","buy_bill")
    ,BUY_IN("buy_in","采购入库","pl_depot_bill","buy_in")
    ,BUY_BACK("buy_back","采购出库","pl_depot_bill","buy_back")
    ,SELL_BILL("sell_bill","销售订单","pl_depot_bill","sell_bill")
    ,SELL_OUT("sell_out","销售出库","pl_depot_bill","sell_out")
    ,SELL_BACK("sell_back","销售退货","pl_depot_bill","sell_back")

    ,DUTY_LEAVE("duty_leave","请假申请","oa_duty_apply","duty_leave")
    ,DUTY_REST("duty_rest","调休申请","oa_duty_apply","duty_rest")
    ,DUTY_OUT("duty_out","外出申请","oa_duty_apply","duty_out")
    ,DUTY_OVERTIME("duty_overtime","加班申请","oa_duty_apply","duty_overtime")
    ,DUTY_OTHERCITYWORK("duty_otherCityWork","出差申请","oa_duty_apply","duty_otherCityWork")
    ,CAR_APPLY("car_apply","车辆申请","oa_car_apply","car_apply")


    ,HOTEL_APPLY("hotel_apply","酒店申请","oa_hotel_apply","hotel_apply")
    ,MEETINGROOM_APPLY("meetingroom_apply","会议室申请","oa_meetingroom_apply","meetingroom_apply")
    ,TICKET_APPLY("ticket_apply","车票申请","oa_ticket_apply","ticket_apply")
    ,SEAL_APPLY("seal_apply","公章申请","oa_seal_apply","seal_apply")

    , CUSTOM_FORM("custom_form","自定义申请表单","oa_custom_form_apply","custom_form");
    /**
     * 单据类型
     */
    private String billType;

    /**
     * 单据名称
     */
    private String billName;

    /**
     * 单据对应表名
     */
    private String tableName;

    /****
     * 启动流程
     */
    private String defKey;

    /**
     * 构造方法
     * @param billType  单据类型
     * @param billName  单据名称
     * @param tableName 单据状态更新类路径
     */
    private BillTypeEnum(String billType, String billName, String tableName,String defKey){
       this.billType = billType;
       this.billName = billName;
       this.tableName= tableName;
       this.defKey = defKey;
    }

    public String getBillType(){
       return billType;
    }

    public String getBillName(){
       return billName;
    }

    public String getTableName() {
        return tableName;
    }

    public String getDefKey() {
        return defKey;
    }

    /**
     *  根据键(billType),获取枚举的值(billName)
     *
     * @param billType 单据编码
     * @return
     */
    public static String getValue(String billType) {
        BillTypeEnum[] BillTypeEnums = values();
        for (BillTypeEnum BillTypeEnum : BillTypeEnums) {
            if (BillTypeEnum.getBillType().equals(billType)) {
                return BillTypeEnum.getBillName();
            }
        }
        return null;
    }


    /**
     *  根据键(billType),获取枚举的值(className)
     *
     * @param billType 单据编码
     * @return
     */
    public static String getTableName(String billType) {
        BillTypeEnum[] classNameEnums = values();
        for (BillTypeEnum classNameEnum : classNameEnums) {
            if (classNameEnum.getBillType().equals(billType)) {
                return classNameEnum.getTableName();
            }
        }
        return null;
    }

    /***
     * 获取启动流程编号
     * @param billType
     * @return
     */
    public static String getDefKey(String billType) {
        BillTypeEnum[] classNameEnums = values();
        for (BillTypeEnum classNameEnum : classNameEnums) {
            if (classNameEnum.getBillType().equals(billType)) {
                return classNameEnum.getDefKey();
            }
        }
        return null;
    }
}
